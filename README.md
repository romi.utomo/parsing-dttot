# About
This repository is code to parse DTTOT file xlsx

# Running Parse
- Place the DDT file wit name `file.xlsx`
- Run `npm install`
- Running the code with `node index.js`
- The `output.csv` file will be created in the directory
    - The file is consist of parsed format file to be imported to BO DTTOT section
-  The `output_original_for_data_modified.csv` file will be created in the directory
    - The file is consist of original data of parsed data that modified such as:
        - Removal of arabic fonts
        - Removal of russian fonts