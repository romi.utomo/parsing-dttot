const Excel = require('exceljs');

function saveExcel(workbook, filename, dir) {
	return new Promise((resolve, reject) => {
		workbook.xlsx.writeFile(dir+filename)
	    .then(function() {
			resolve({path : dir+filename, filename : filename});
	    }).catch((error) => {
			console.log(error)
			return reject('failed to write excel')
		});
	})
}

function readXLSX(filename) {
	return new Promise((resolve, reject) => {
		var workbook = new Excel.Workbook();
		workbook.xlsx.readFile(filename)
		.then(function() {
			let output = parseSingleSheet(workbook)
			resolve(output)
		}).catch((error) => {
			console.log(error)
			return reject(error)
		})
	})
}

function saveCSV(workbook, filename, dir) {
	return new Promise((resolve, reject) => {
		workbook.csv.writeFile(dir+filename)
	    .then(function() {
			resolve({path : dir+filename, filename : filename});
	    }).catch((error) => {
			console.log(error)
			return reject('failed to write csv')
		});
	})
}

function parseSingleSheet(workbook) {
	let output = []
	let sheetList = []

	workbook.eachSheet(function(worksheet, sheetId) {
	    sheetList.push(sheetId);
	});

	if (sheetList.length == 0) {
		return output
	}

	let worksheet = workbook.getWorksheet(sheetList[0])
	worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
		if (row.values.length > 0) {
			output.push(row.values)
		}
	})

	return output
}


function createSingleSheet(workbook, sheetName, header, values, headerStyle = {}) {
	var worksheet = workbook.addWorksheet(sheetName);
	worksheet.state = 'show';
	worksheet.columns = header;
	worksheet.getRow(1).font = headerStyle;
	worksheet.addRows(values);

	return workbook
}

function createCSVSingleSheet(header, values, filename, dir){
    return new Promise((resolve, reject) => {			
        var workbook = new Excel.Workbook();

        workbook.creator = 'Bibit';
        workbook.created = new Date();
        workbook = createSingleSheet(workbook, 'Sheet1', header, values)

        saveCSV(workbook, filename, dir).then((response) =>{
            resolve(response)
        }).catch((error) => {
            return reject(error)
        })
    })
}

module.exports = {
    readXLSX,
    saveExcel,
    createCSVSingleSheet
}