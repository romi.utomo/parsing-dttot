const excel = require('./lib/excel')

const pathFile = 'file.xlsx'

function cleanString(input) {
    var output = "";
    let isParsed = false
    input = input.replace(/`/g, `'`)
    .replace(/'/g, `'`)
    .replace(/’/g, `'`)
    .replace(/‘/g, `'`)
    for (var i=0; i<input.length; i++) {
        if (input.charCodeAt(i) <= 127) {
            output += input.charAt(i);
        } else {
            isParsed = true
        }
    }
    return { name: output, isParsed };
}

function containsAnyLetter(str) {
    return /[a-zA-Z]/.test(str);
  }

function csvObjectFormat(name, splitTerrorist, data) {
    const terroristObj = {
        name: name.trim(),
    }
    for (let i = 0; i < splitTerrorist.length; i++) {
        //validation utf-8
        terroristObj[`alias${i}`] = splitTerrorist[i].trim()
    }
    data.push(terroristObj)
}

async function main () {
    let terrorists =  await excel.readXLSX(pathFile)
    terrorists = terrorists.slice(1)
    const data = []
    const dataParsedAll = []
    for (terrorist of terrorists) {
        const dataParsed = []
        const separators = ['alias', 'ALIAS', 'Alias', 'a.k.a.'];
        let splitTerrorist = terrorist[1].split(new RegExp(separators.join('|'),'g'));

        const {name, isParsed } = cleanString(splitTerrorist[0])
        if (isParsed) {
            dataParsed.push(splitTerrorist[0])
        }


        splitTerrorist = splitTerrorist.slice(0)
            .map(val => {
                let {name, isParsed } = cleanString(val)
                name = name.replace(';', '');

                if (isParsed) {
                    if (dataParsed.length === 0) {
                        dataParsed.push(splitTerrorist[0])
                    } 
                    dataParsed.push(val)
                }
                return name
            })
            .filter(val => containsAnyLetter(val))


        csvObjectFormat(name, splitTerrorist, data)
        if (dataParsed[0]) csvObjectFormat(dataParsed[0], dataParsed.slice(0), dataParsedAll)
     
    }
    const headers = [
        {header: 'name', key: 'name'}
    ]
    for (let i = 1; i <= 39; i++) {
       headers.push({ header: `alias${i}`, key: `alias${i}`})
    }

    await excel.createCSVSingleSheet(headers, data, 'output.csv', '')
    await excel.createCSVSingleSheet(headers, dataParsedAll, 'output_original_for_data_modified.csv', '')
}

main()